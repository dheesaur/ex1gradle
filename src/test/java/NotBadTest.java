import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class NotBadTest {
    private NotBad notBadTestObject = new NotBad();
    @Test
    @DisplayName("NotBadTest")
    void findNotAndBadStandardTest() {
        assertEquals("thats good!", notBadTestObject.findNotAndBad("thats not too bad!"));
    }

    @Test
    @DisplayName("NotBadTest, multiple not and bad in input string")
    void findNotBadMultipleNotBadsInputTest() {
        assertEquals("hey thats bad good not", notBadTestObject.findNotAndBad("hey thats bad not that bad not"));
    }

    @Test
    @DisplayName("NotBadTest, Bad occurs before Not in Input String")
    void findNotBadBadBeforeNotInputTest() {
        assertEquals("", notBadTestObject.findNotAndBad( "this is bad but not the worst!"));
    }

    @Test
    @DisplayName("NotBadTest, No \"Bad\" or \"Not\" in Input String")
    void findNotBadNoBadOrNotInputTest() {
        assertEquals("check string", notBadTestObject.findNotAndBad("quick brown fox jumper over the lazy dog"));
    }
}