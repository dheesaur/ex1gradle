import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class BothEndsTest {
    private BothEnds bothEndsTestObject = new BothEnds();
    @Test
    @DisplayName("BothEnds Test: Example in exercise file")
    void getBothEndsOfStringStandardTest() {
        assertEquals( "Sper", bothEndsTestObject.getBothEndsOfString("Springer"));
    }

    @Test
    @DisplayName("BothEnds Test: Input String Size less than 2")
    void getbothEndsOfStringInputSizeTest() {
        assertEquals("", bothEndsTestObject.getBothEndsOfString("s" ));
    }

    @Test
    @DisplayName("BothEnds Test: Input String Size is 2")
    void getBothEndsOfStringInputSizeLessThan2Test() {
        assertEquals("Sp", bothEndsTestObject.getBothEndsOfString("Sp"));
    }
}