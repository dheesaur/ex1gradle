import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MixupTest {
    private Mixup mixupTestObject = new Mixup();
    @Test
    @DisplayName("donutCount Test for Example input in Exercise Files")
    void stringMixerStandardTest() {
        assertEquals("pox mid", mixupTestObject.stringMixer("mix", "pod"));
        assertEquals("dig donner", mixupTestObject.stringMixer("dog", "dinner"));
    }

    @Test
    @DisplayName("donutCount Test for Space in input")
    void stringMixerInputSpaceTest() {
        assertEquals("kea da avda vra", mixupTestObject.stringMixer("ava da", "keda vra"));
    }

    @Test
    @DisplayName("donutCount Test for null in string inputs")
    void stringMixerEmptyStringInputTest() {
        assertNull(mixupTestObject.stringMixer("mix", ""));
        assertNull(mixupTestObject.stringMixer("", ""));
    }
}