import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DonutCountTest {
    private Donut donutCountTestObject = new Donut();
    @Test
    @DisplayName("Test for Example input provided in Exercise file")
    void donutCounterStandardTest() {
        assertEquals("The number of Donuts is many", donutCountTestObject.donutCounter(23));
        assertEquals("The number of Donuts is 5", donutCountTestObject.donutCounter(5));
    }

    @Test
    @DisplayName("Input number is 10")
    void donutCounterInput10Test() {
        assertEquals("The number of Donuts is many", donutCountTestObject.donutCounter(10));
    }

    @Test
    @DisplayName("Input is negative")
    void donutCounterInputNegativeTest() {
        assertEquals("The number of Donuts is -1", donutCountTestObject.donutCounter(-1));
    }
}
