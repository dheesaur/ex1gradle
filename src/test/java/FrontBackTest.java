import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FrontBackTest {
    private FrontBack frontBackTestObject = new FrontBack();
    @Test
    @DisplayName("FrontBack, Example case from exercise")
    void getFrontAndBackStandardTest() {
        assertEquals("abxcdy", frontBackTestObject.getFrontAndBack("abcd", "xy"));
    }

    @Test
    @DisplayName("FrontBack, Spaces in input String test")
    void getFrontAndBackInputSpaceTest() {
        assertEquals("ab xycd z", frontBackTestObject.getFrontAndBack("ab cd", "xy z" ));
    }

    @Test
    @DisplayName("FrontBack, Both input Strings are of Odd length")
    void getFrontAndBackOddInputTest() {
        assertEquals("abcxydez", frontBackTestObject.getFrontAndBack("abcde", "xyz"));
    }

    @Test
    @DisplayName("FrontBack, Both input Strings are of even length")
    void getFrontAndBackEvenInputTest() {
        assertEquals("001001", frontBackTestObject.getFrontAndBack("0000", "11"));
    }

    @Test
    @DisplayName("FrontBack, One input string is empty")
    void getFrontAndBackEmptyInputTest() {
        assertEquals("abcd", frontBackTestObject.getFrontAndBack("abcd", ""));
    }

}