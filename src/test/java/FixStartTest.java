import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FixStartTest {
    private FixStart fixStartTestObject = new FixStart();
    @Test
    @DisplayName("FixStart Example Input Test")
    void changeCharacterToXStandardTest() {
        assertEquals("ba**le", fixStartTestObject.changeCharacterToX("babble"));
    }

    @Test
    @DisplayName("FixStart, First Character isnt repeated in the input string")
    void changeCharacterToXNoMatchTest() {
        assertEquals("banana", fixStartTestObject.changeCharacterToX("banana"));
    }

    @Test
    @DisplayName("FixStart, First Character is Space")
    void changeCharacterToXSpaceTest() {
        assertEquals(" this*is*a*sample*test", fixStartTestObject.changeCharacterToX(" this is a sample test"));
    }
}