import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class VerbingTest {
    private Verbing verbingTestObject = new Verbing();
    @Test
    @DisplayName("VerbingTest")
    void doVerbingStandardTest() {
        assertEquals("suryasofting", verbingTestObject.doVerbing("suryasoft"));
    }

    @Test
    @DisplayName("Verbing, Test for input String Ending in \"ing\"")
    void doVerbingInputEndsIngTest() {
        assertEquals("runnly", verbingTestObject.doVerbing("running"));
    }

    @Test
    @DisplayName("Verbing, Test for input String size < 3 and is ing")
    void doVerbingInputIsIng() {
        assertEquals("ing", verbingTestObject.doVerbing("ing"));
    }

    @Test
    @DisplayName("Verbing, Test for input size less than 3")
    void doVerbingInputSizeLessThan3() {
        assertEquals("qw", verbingTestObject.doVerbing("qw"));
    }
}