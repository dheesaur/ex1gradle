import org.jetbrains.annotations.NotNull;

class FixStart {
    private String fixStartResult;
    //Method searches for occurrences of first character in the input string and changes them all to '*'
    @NotNull String changeCharacterToX(@NotNull String input) {
        char firstCharacterOfInput = input.charAt(0);
        char[] inputCharacterArray = input.toCharArray();
        for (int i = 1; i<input.length(); i++) {
            if (inputCharacterArray[i] == firstCharacterOfInput) {
                inputCharacterArray[i] = '*';
            }
            fixStartResult = String.valueOf(inputCharacterArray);
        }
        return fixStartResult;
    }
}
