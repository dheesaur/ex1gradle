import org.jetbrains.annotations.NotNull;

class Donut {
    //Given an input returns "Number of Donuts is input" or "Number of Donuts is Many"
    @NotNull String donutCounter(int numberOfDonuts) {
        String donutCounterResult;
        if (numberOfDonuts < 10) {
            donutCounterResult = "The number of Donuts is" + " " + numberOfDonuts;
        } else {
            donutCounterResult = "The number of Donuts is many";
        }
        return donutCounterResult;
    }
}