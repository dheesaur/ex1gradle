public class ExerciseMain{
    public static void main(String args[]) {
        System.out.println("Java Exercise1\n");
        ExerciseMain newMainObject = new ExerciseMain();

        //1.Donut Count:
        System.out.println("DonutCount : Given an input returns \"Number of Donuts is input\" or \""
                + "Number of Donuts is Many\"");
        int numberOfDonuts = -1;
        System.out.println(newMainObject.donutCountMain(numberOfDonuts));
        System.out.println();

        //2.Both Ends:
        System.out.println("Both Ends:Given an input returns first and last two characters, if " +
                "input.length<2 returns empty ");
        String bothEndInput = "Springer";
        System.out.println(newMainObject.bothEndsMain(bothEndInput));
        System.out.println();

        //3.Fix Start:
        System.out.println("Fix Start : Given an input string returns the string with character " +
                "matching the first character changed to \"*\"");
        String fixStartInput = "blubber";
        System.out.println(newMainObject.fixStartMain(fixStartInput));
        System.out.println();

        //4.Mixup:
        System.out.println("Mixup : Given two strings, returns a string with the first characters "
                + "of the two strings exchanged");
        String targetMixup1 = "dog", targetMixup2 = "mix";
        System.out.println(newMainObject.mixupMain(targetMixup1, targetMixup2));
        System.out.println();

        //5.Verbing:
        System.out.println("Verbing : If length of input string is more than 3, add \"ly\" to the" +
                " end if length is more than 3 and has \"ly\", \"ly\" is replaced with \"ing\"");
        String targetVerbing = "running";
        System.out.println(newMainObject.verbingMain((targetVerbing)));
        System.out.println();

        //6.Not Bad:
        System.out.println("NotBad : Searches for \"not\" and \"bad\" in input string and if not " +
                "occurs before bad, replace the whole \"not ... bad\" substring with \"good\"");
        String targetString = "this is actually not that bad!";
        System.out.println(newMainObject.notBadMain(targetString));

        //7.Front and Back:
        System.out.println("Front and Back : Given two inputs, splits them and returns new string");
        String sampleInputA = "ab cd", sampleInputB = "xy z";
        System.out.println(newMainObject.frontAndBackMain(sampleInputA, sampleInputB));

    }

    //DonutCount, Given an input returns "Number of Donuts is input" or "Number of Donuts is Many"
    private  String donutCountMain(int number) {
        String donutResult;
        Donut freshDonut = new Donut();
        donutResult = freshDonut.donutCounter(number);
        return donutResult;
    }

    //BothEnds, Given an input returns first and last two characters, if input.length<2 returns empty
    private String bothEndsMain(String inputString) {
        BothEnds newString = new BothEnds();
        String bothEndResult;
        bothEndResult = newString.getBothEndsOfString(inputString);
        return bothEndResult;
    }

    //FixStart, Given an input string returns the string with character matching the first character
    // changed to "*"
    private String fixStartMain(String inputFixStart) {
        FixStart newWord = new FixStart();
        String resultFixStart;
        resultFixStart = newWord.changeCharacterToX(inputFixStart);
        return resultFixStart;
    }

    //Mixup, Given two strings, returns a string with the first characters of the two strings
    // exchanged
    private String mixupMain(String inputMixing1, String inputMixing2) {
        Mixup twoWords = new Mixup();
        String resultMixup;
        resultMixup = twoWords.stringMixer(inputMixing1, inputMixing2);
        return resultMixup;
    }

    //Verbing, If length of input string is more than 3, add "ly" to the end, if length is more than
    // 3 and has "ly", "ly" is replaced with "ing"
    private String verbingMain(String inputVerbing){
        Verbing stringToBeVerbed = new Verbing();
        String resultVerbing;
        resultVerbing = stringToBeVerbed.doVerbing(inputVerbing);
        return resultVerbing;
    }

    //NotBad, Searches for "not" and "bad" in input string and if not occurs before bad, replace the
    // whole "not ... bad" substring with "good"
    private String notBadMain(String inputNotBad){
        NotBad objectNotBad = new NotBad();
        String resultNotBad;
        resultNotBad = objectNotBad.findNotAndBad(inputNotBad);
        return resultNotBad;
    }

    //FrontAndBack, Given two inputs, splits them and returns new string
    private String frontAndBackMain(String inputStringA, String inputStringB){
        FrontBack stringFrontAndBack = new FrontBack();
        String resultFrontBack;
        resultFrontBack = stringFrontAndBack.getFrontAndBack(inputStringA, inputStringB);
        return resultFrontBack;
    }
}
