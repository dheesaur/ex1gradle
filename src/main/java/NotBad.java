import org.jetbrains.annotations.Nullable;

class NotBad  {
    private String findNotBadResult;
    //Searches for "not" and "bad" in input string and if not occurs before bad, replace the
    //whole "not ... bad" substring with "good"
    @Nullable String findNotAndBad(String string1) {
        int lengthOfInput = string1.length();
        int indexOfNot = string1.indexOf("not");
        int indexOfBad = string1.lastIndexOf("bad");
        if (indexOfNot > indexOfBad) {
            findNotBadResult = "";
        } else if (indexOfNot < indexOfBad) {
            findNotBadResult = string1.substring( 0, indexOfNot ) + "good" + string1.substring( indexOfBad + 3, lengthOfInput );
        } else if (indexOfNot == -1 || indexOfBad == -1) {
            findNotBadResult = "check string";
        }
        return findNotBadResult;
    }
}

