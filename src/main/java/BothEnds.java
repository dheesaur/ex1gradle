import org.jetbrains.annotations.NotNull;

class BothEnds {
    //Method returns the first and last two characters of input string as result string
    @NotNull String getBothEndsOfString(@NotNull String input) {
        String bothEndsResult;
        int lengthOfInput = input.length();
        //if input string's length is more than 2, sub
        if (lengthOfInput > 2) {
            bothEndsResult = input.substring(0, 2) + input.substring(lengthOfInput - 2, lengthOfInput);
        } else if (lengthOfInput < 2) {
            bothEndsResult = "";
        } else {
            bothEndsResult = input;
        }
        return bothEndsResult;
    }
}
