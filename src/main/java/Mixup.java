import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

class Mixup {
    //Method switches the first characters of two input strings
    @Nullable String stringMixer(@NotNull String inputA, @NotNull String inputB) {
        String stringMixerResult;
        int lengthOfInputA = inputA.length();
        int lengthOfInputB = inputB.length();
        if ((lengthOfInputB == 0) || (lengthOfInputA == 0)) {
            stringMixerResult = null;
        } else {
            stringMixerResult = inputB.substring(0,2) + inputA.substring(2, lengthOfInputA) + " " + inputA.substring(0,2) + inputB.substring(2,lengthOfInputB);
        }
        return stringMixerResult;
    }
}