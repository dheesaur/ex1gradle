import org.jetbrains.annotations.NotNull;

class Verbing {
    private String doVerbingResult;
    //Method adds 'ing' to input string, if it doesnt end with ing, else replaces with 'ly'
    @NotNull String doVerbing(@NotNull String input) {
        int lengthOfInput = input.length();
        if (lengthOfInput <= 3 ) {
            doVerbingResult = input;
        } else if (input.endsWith("ing")) {
            doVerbingResult = input.substring(0, lengthOfInput - 3);
            doVerbingResult = doVerbingResult+"ly";
        } else if (!input.endsWith("ing")) {
            doVerbingResult = input+"ing";
        }
        return doVerbingResult;
    }
}

