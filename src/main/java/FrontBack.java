import org.jetbrains.annotations.NotNull;

class FrontBack {
    //Method splits the front and back of the input strings and gives new string
    @NotNull String getFrontAndBack(@NotNull String inputA, @NotNull String inputB) {
        String frontA, frontB, backA, backB, getFrontAndBackResult;
        int lengthA = inputA.length();
        int lengthB = inputB.length();
        if (lengthA%2 == 0) {
            frontA = inputA.substring(0, lengthA/2);
            backA = inputA.substring(lengthA/2, lengthA);
        } else {
            frontA = inputA.substring(0, lengthA/2 + 1);
            backA = inputA.substring(lengthA/2+1, lengthA);
        }
        if (lengthB%2 == 0) {
            frontB = inputB.substring(0, lengthB/2);
            backB = inputB.substring(lengthB/2, lengthB);
        } else {
            frontB = inputB.substring(0, lengthB/2 + 1);
            backB = inputB.substring(lengthB/2 + 1, lengthB);
        }
        getFrontAndBackResult = frontA + frontB + backA + backB;
        return getFrontAndBackResult;
    }
}
